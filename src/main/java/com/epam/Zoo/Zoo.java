package com.epam.Zoo;

import com.epam.Zoo.Animal.Bird;
import com.epam.Zoo.Animal.Eagle;
import com.epam.Zoo.Animal.Fish;
import com.epam.Zoo.Animal.Parrot;


public class Zoo {

    public static void main(String[] args) {
        Fish fishZoo = new Fish();
        Bird birdZoo = new Bird();
        Parrot parrotZoo = new Parrot();
        Eagle eagleZoo = new Eagle();

        fishZoo.executeAction();
        birdZoo.executeAction();
        parrotZoo.executeAction();
        eagleZoo.executeAction();
    }
}
