package com.epam.Zoo.Animal;

import com.epam.Helper.AbstractPage;

public class Eagle extends AbstractPage {

    @Override
    public void executeAction() {
        System.out.println("Eagle: " + "Eat meat!");
        System.out.println("Fly!" + "\n");
    }
}
