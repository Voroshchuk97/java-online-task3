package com.epam.Shape;

import com.epam.Shape.Forms.Circle;
import com.epam.Shape.Forms.Rectangle;
import com.epam.Shape.Forms.Triangle;


public class Shape {

    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.executeAction();
        Rectangle rectangle = new Rectangle();
        rectangle.executeAction();
        Triangle triangle = new Triangle();
        triangle.executeAction();

    }
}
