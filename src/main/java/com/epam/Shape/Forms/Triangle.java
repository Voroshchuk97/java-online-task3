package com.epam.Shape.Forms;

import com.epam.Helper.AbstractPage;

public class Triangle extends AbstractPage {
    private int sideA = 8;
    private int sideB = 8;
    private int sideC = 8;

    @Override
    public void executeAction() {
        int sum = ((sideA + sideB + sideC) / 2);
        double sum1 = Math.sqrt(((sum - sideA) * (sum - sideB) * (sum - sideC)));
        System.out.println("Triangle colore: " + "Purple | " + "area of the figure = " + sum1);
    }

}
