package com.epam.Shape.Forms;

import com.epam.Helper.AbstractPage;

public class Circle extends AbstractPage {
    private final double P = 3.14159265359;
    private double lengthOfSide = 15;

    @Override
    public void executeAction() {
        lengthOfSide = (Math.pow((lengthOfSide / (2 * P)), 2)) * P;
        System.out.println("Circle colore: " + "Red | " + "area of the figure = " + lengthOfSide);
    }
}
